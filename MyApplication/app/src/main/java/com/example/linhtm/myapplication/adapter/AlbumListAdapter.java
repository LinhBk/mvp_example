package com.example.linhtm.myapplication.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.linhtm.myapplication.model.album.Album;

import java.util.List;

/**
 * Created by LinhTM on 1/9/2017.
 */

public class AlbumListAdapter extends RecyclerView.Adapter<AlbumHolder> {
    private List<Album> list;
    public AlbumListAdapter(List<Album> list){
        this.list = list;
    }
    @Override
    public AlbumHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AlbumHolder(LayoutInflater.from(parent.getContext()),parent);
    }

    @Override
    public void onBindViewHolder(AlbumHolder holder, int position) {
        Album album = list.get(position);
        holder.artistName.setText("Artist: " + album.getArtistName());
        holder.albumName.setText(album.getAlbumName());
        holder.totalTracks.setText("Total tracks: " + album.getTotalTrack());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
