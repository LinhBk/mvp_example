package com.example.linhtm.myapplication.presenter;

import android.content.Context;

import com.example.linhtm.myapplication.model.OnLoadData;
import com.example.linhtm.myapplication.model.album.Album;
import com.example.linhtm.myapplication.model.album.AlbumListHelper;
import com.example.linhtm.myapplication.model.track.Track;
import com.example.linhtm.myapplication.view.AlbumView;

import java.util.List;

/**
 * Created by LinhTM on 1/9/2017.
 */

public class AlbumPresenter implements OnLoadData {
    private AlbumListHelper model;
    private AlbumView view;

    public AlbumPresenter(AlbumListHelper model, AlbumView view){
        this.model = model;
        this.view = view;
        model.setOnLoadData(this);
    }

    public void getData(Context context){
        model.getData(context);
    }

    @Override
    public void onLoadDataSuccess(List<Track> trackList, List<Album> albumList) {
        if(albumList.size() > 0){
            view.display(albumList);
        } else{
            view.showNoData();
        }
    }

    @Override
    public void onLoadDataError() {
        view.showError();
    }
}
