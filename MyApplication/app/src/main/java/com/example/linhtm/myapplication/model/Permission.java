package com.example.linhtm.myapplication.model;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.widget.Toast;

import static android.support.v4.app.ActivityCompat.requestPermissions;
import static android.support.v4.app.ActivityCompat.shouldShowRequestPermissionRationale;

/**
 * Created by LinhTM on 1/10/2017.
 */

public class Permission {
    public static int permissionGranted = -2;
    public static int initPermission(Context context, Activity activity){
        if(permissionGranted == -2) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    if (shouldShowRequestPermissionRationale(activity,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(context, "Permission isn't granted ", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Permisson don't granted and dont show dialog again ", Toast.LENGTH_SHORT).show();
                    }
                    requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                    permissionGranted = -1;
                    return -1;
                } else {
                    permissionGranted = 1;
                    return 1;
                }
            }
            permissionGranted = 0;
            return 0;
        }
        return permissionGranted;
    }
}
