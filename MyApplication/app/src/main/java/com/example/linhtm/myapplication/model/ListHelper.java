package com.example.linhtm.myapplication.model;

import android.content.Context;

/**
 * Created by LinhTM on 1/9/2017.
 */

public interface ListHelper {
    void setOnLoadData(OnLoadData onLoadData);
    void getData(Context context);
}
