package com.example.linhtm.myapplication.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.linhtm.myapplication.model.track.Track;

import java.util.List;

/**
 * Created by LinhTM on 1/6/2017.
 */

public class TrackListAdapter extends RecyclerView.Adapter<TrackHolder> {
    private List<Track> list;
    public TrackListAdapter(List<Track> list){
        this.list = list;
    }
    @Override
    public TrackHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TrackHolder(LayoutInflater.from(parent.getContext()),parent);
    }

    @Override
    public void onBindViewHolder(TrackHolder holder, int position) {
        Track track = list.get(position);
        holder.artistName.setText("Artist: "+ track.getArtistName());
        holder.albumName.setText("Album: "+track.getAlbumName());
        holder.trackName.setText(track.getTrackName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
