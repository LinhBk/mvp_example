package com.example.linhtm.myapplication.view;

import com.example.linhtm.myapplication.model.track.Track;

import java.util.List;

/**
 * Created by LinhTM on 1/6/2017.
 */

public interface TrackView {
    void showNoData();
    void showError();
    void display(List<Track> list);
}
