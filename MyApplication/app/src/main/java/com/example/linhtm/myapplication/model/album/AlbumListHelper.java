package com.example.linhtm.myapplication.model.album;

import android.content.Context;

import com.example.linhtm.myapplication.model.ListHelper;
import com.example.linhtm.myapplication.model.OnLoadData;
import com.example.linhtm.myapplication.support.GetAlbumInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LinhTM on 1/9/2017.
 */

public class AlbumListHelper implements ListHelper {
    private List<Album> albumList;
    private OnLoadData onLoadData;

    @Override
    public void setOnLoadData(OnLoadData onLoadData) {
        this.onLoadData = onLoadData;
    }

    @Override
    public void getData(Context context) {
        if(albumList == null){
            albumList = new ArrayList<>();
            albumList = GetAlbumInfo.getAlbumInfo(context);
            if(albumList == null){
                onLoadData.onLoadDataError();
            }
        }
        onLoadData.onLoadDataSuccess(null,albumList);
    }

    public void setAlbumList(List<Album> albumList) {
        this.albumList = albumList;
    }
}
