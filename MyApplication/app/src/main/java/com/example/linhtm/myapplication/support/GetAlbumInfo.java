package com.example.linhtm.myapplication.support;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.example.linhtm.myapplication.model.album.Album;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LinhTM on 1/9/2017.
 */

public class GetAlbumInfo {
    public GetAlbumInfo(){

    }
    public static List<Album> getAlbumInfo(Context context){
        List<Album> list = new ArrayList<>();

        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Albums.ALBUM +" !=0" ;
        String[] START = {" * "};
        Cursor cursor = context.getContentResolver().query(uri,START,selection,null,null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do{
                    String albumName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM));
                    String artistName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST));
                    String totalTracks = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS));
                    Album album = new Album(albumName,artistName,totalTracks);
                    list.add(album);
                } while (cursor.moveToNext());
            }
        }
        return list;
    }
}
