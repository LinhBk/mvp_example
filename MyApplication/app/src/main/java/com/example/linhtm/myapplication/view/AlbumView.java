package com.example.linhtm.myapplication.view;

import com.example.linhtm.myapplication.model.album.Album;

import java.util.List;

/**
 * Created by LinhTM on 1/9/2017.
 */

public interface AlbumView {
    void showError();
    void showNoData();
    void display(List<Album> albumList);
}
