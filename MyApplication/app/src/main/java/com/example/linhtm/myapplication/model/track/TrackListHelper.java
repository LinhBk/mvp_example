package com.example.linhtm.myapplication.model.track;

import android.content.Context;

import com.example.linhtm.myapplication.model.ListHelper;
import com.example.linhtm.myapplication.model.OnLoadData;
import com.example.linhtm.myapplication.model.track.Track;
import com.example.linhtm.myapplication.support.GetTrackInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LinhTM on 1/6/2017.
 */

public class TrackListHelper implements ListHelper {
    private List<Track> trackList;
    private OnLoadData onLoadData;

    public void setTrackList(List<Track> trackList){
        this.trackList = trackList;
    }

    public void setOnLoadData(OnLoadData onLoadData) {
        this.onLoadData = onLoadData;
    }
    public void getData(Context context){
        if(trackList == null){
            trackList = new ArrayList<>();
            trackList = GetTrackInfo.getTrackInfo(context);
            if(trackList == null){
                onLoadData.onLoadDataError();
                return;
            }
        }

        onLoadData.onLoadDataSuccess(trackList,null);
    }
}
