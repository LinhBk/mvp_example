package com.example.linhtm.myapplication.model;

import com.example.linhtm.myapplication.model.album.Album;
import com.example.linhtm.myapplication.model.track.Track;

import java.util.List;

/**
 * Created by LinhTM on 1/6/2017.
 */

public interface OnLoadData {
    void onLoadDataSuccess(List<Track> trackList, List<Album> albumList);
    void onLoadDataError();
}
