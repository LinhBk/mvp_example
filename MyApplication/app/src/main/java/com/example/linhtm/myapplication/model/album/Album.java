package com.example.linhtm.myapplication.model.album;

/**
 * Created by LinhTM on 1/9/2017.
 */

public class Album {
    private String albumName;
    private String artistName;
    private String totalTracks;

    public Album(String albumName,String artistName,String totalTracks){
        this.artistName = artistName;
        this.albumName = albumName;
        this.totalTracks = totalTracks;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getTotalTrack() {
        return totalTracks;
    }
}
