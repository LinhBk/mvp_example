package com.example.linhtm.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.linhtm.myapplication.R;
import com.example.linhtm.myapplication.adapter.TrackHolder;
import com.example.linhtm.myapplication.adapter.TrackListAdapter;
import com.example.linhtm.myapplication.model.Permission;
import com.example.linhtm.myapplication.model.track.Track;
import com.example.linhtm.myapplication.model.track.TrackListHelper;
import com.example.linhtm.myapplication.presenter.TrackPresenter;
import com.example.linhtm.myapplication.view.TrackView;

import java.util.List;

/**
 * Created by LinhTM on 1/9/2017.
 */

public class TabTrack extends Fragment implements TrackView {
    private RecyclerView recyclerView;
    private TrackListAdapter adapter;
    private List<Track> list;
    private TrackPresenter presenter;
    private TrackListHelper model;
    private TextView tvNoData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(presenter == null){
            if(model == null){
                model = new TrackListHelper();
            }
            model.setTrackList(list);
            presenter = new TrackPresenter(model,this);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNoData = (TextView)view.findViewById(R.id.fragment_noData);
        recyclerView = (RecyclerView)view.findViewById(R.id.fragment_list);
        recyclerView.setLayoutManager( new LinearLayoutManager(getContext()));

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(Permission.initPermission(getContext(),getActivity()) != -1) {
            if (presenter != null)
                presenter.getData(getContext());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_fragment,container,false);
    }

    @Override
    public void showNoData() {
        tvNoData.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showError() {
        Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();
        tvNoData.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void display(List<Track> list) {
        tvNoData.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
        adapter = new TrackListAdapter(list);
        recyclerView.setAdapter(adapter);
    }
}
