package com.example.linhtm.myapplication.model.track;

/**
 * Created by LinhTM on 1/6/2017.
 */

public class Track {
    private String trackName;
    private String artistName;
    private String albumName;
    public Track(String trackName,String artistName, String albumName){
        this.trackName = trackName;
        this.albumName = albumName;
        this.artistName = artistName;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getAlbumName() {
        return albumName;
    }
}
