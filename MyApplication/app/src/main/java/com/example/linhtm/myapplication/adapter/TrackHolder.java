package com.example.linhtm.myapplication.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.linhtm.myapplication.R;

/**
 * Created by LinhTM on 1/6/2017.
 */

public class TrackHolder extends RecyclerView.ViewHolder{
    public TextView trackName;
    public TextView albumName;
    public TextView artistName;

    public TrackHolder(View itemView) {
        super(itemView);
    }
    public TrackHolder(LayoutInflater layoutInflater, ViewGroup parent){
        super(layoutInflater.inflate(R.layout.track_item,parent,false));
        trackName = (TextView)itemView.findViewById(R.id.trackItem_trackName);
        albumName = (TextView)itemView.findViewById(R.id.trackItem_albumName);
        artistName = (TextView)itemView.findViewById(R.id.trackItem_artistName);
    }
}
