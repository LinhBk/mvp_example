package com.example.linhtm.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.linhtm.myapplication.adapter.AlbumListAdapter;
import com.example.linhtm.myapplication.model.Permission;
import com.example.linhtm.myapplication.model.album.Album;
import com.example.linhtm.myapplication.model.album.AlbumListHelper;
import com.example.linhtm.myapplication.presenter.AlbumPresenter;
import com.example.linhtm.myapplication.view.AlbumView;

import java.util.List;

/**
 * Created by LinhTM on 1/9/2017.
 */

public class TabAlbum extends Fragment implements AlbumView {
    private List<Album> list;

    private RecyclerView recyclerView;
    private AlbumListAdapter adapter;
    private AlbumListHelper model;
    private AlbumPresenter presenter;
    private TextView tvNoData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(presenter == null){
            if(model == null){
                model = new AlbumListHelper();
            }
            model.setAlbumList(list);
            presenter = new AlbumPresenter(model,this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNoData = (TextView)view.findViewById(R.id.fragment_noData);
        recyclerView = (RecyclerView)view.findViewById(R.id.fragment_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(Permission.initPermission(getContext(),getActivity()) != -1) {
            if (presenter != null)
                presenter.getData(getContext());
        }
    }

    @Override
    public void showError() {
        Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();
        recyclerView.setVisibility(View.INVISIBLE);
        tvNoData.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showNoData() {
        recyclerView.setVisibility(View.INVISIBLE);
        tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public void display(List<Album> albumList) {
        recyclerView.setVisibility(View.VISIBLE);
        tvNoData.setVisibility(View.INVISIBLE);
        adapter = new AlbumListAdapter(albumList);
        recyclerView.setAdapter(adapter);
    }
}
