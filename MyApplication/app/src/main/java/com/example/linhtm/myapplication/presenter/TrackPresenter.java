package com.example.linhtm.myapplication.presenter;

import android.content.Context;
import android.util.Log;

import com.example.linhtm.myapplication.model.ListHelper;
import com.example.linhtm.myapplication.model.OnLoadData;
import com.example.linhtm.myapplication.model.album.Album;
import com.example.linhtm.myapplication.model.track.Track;
import com.example.linhtm.myapplication.model.track.TrackListHelper;
import com.example.linhtm.myapplication.view.TrackView;

import java.util.List;

/**
 * Created by LinhTM on 1/6/2017.
 */

public class TrackPresenter implements OnLoadData {

    private TrackListHelper trackListHelper;
    private TrackView trackView;

    public TrackPresenter(TrackListHelper model, TrackView view){
        this.trackListHelper = model;
        this.trackView = view;
        trackListHelper.setOnLoadData(this);
    }

    public void getData(Context context){
        trackListHelper.getData(context);
    }

    @Override
    public void onLoadDataSuccess(List<Track> trackList, List<Album> albumList) {
        if(trackList.size() >0){
            trackView.display(trackList);
        } else{
            trackView.showNoData();
        }
    }

    @Override
    public void onLoadDataError() {
        trackView.showError();
    }
}
