package com.example.linhtm.myapplication;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.linhtm.myapplication.adapter.Adapter;



public class MainActivity extends AppCompatActivity{

    private Toolbar topToolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        topToolbar = (Toolbar)findViewById(R.id.mainac_topToolbar);
        tabLayout = (TabLayout)findViewById(R.id.mainac_tabBar);
        viewPager = (ViewPager)findViewById(R.id.mainac_viewPager);

        setSupportActionBar(topToolbar);
        adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new TabTrack(), "Track");
        adapter.addFragment(new TabAlbum(), "Album");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
