package com.example.linhtm.myapplication.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.linhtm.myapplication.R;

/**
 * Created by LinhTM on 1/9/2017.
 */

public class AlbumHolder extends RecyclerView.ViewHolder {
    public TextView albumName;
    public TextView artistName;
    public TextView totalTracks;

    public AlbumHolder(View itemView) {
        super(itemView);
    }
    public AlbumHolder(LayoutInflater layoutInflater, ViewGroup viewGroup){
        super(layoutInflater.inflate(R.layout.album_item,viewGroup,false));
        albumName = (TextView)itemView.findViewById(R.id.albumItem_albumName);
        artistName = (TextView)itemView.findViewById(R.id.albumItem_artistName);
        totalTracks = (TextView)itemView.findViewById(R.id.albumItem_totalTracks);
    }
}
