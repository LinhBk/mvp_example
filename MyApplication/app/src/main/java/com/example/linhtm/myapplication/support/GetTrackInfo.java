package com.example.linhtm.myapplication.support;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.example.linhtm.myapplication.model.track.Track;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LinhTM on 1/9/2017.
 */

public class GetTrackInfo {
    public GetTrackInfo(){

    }
    public static List<Track> getTrackInfo(Context context){
        List<Track> trackList = new ArrayList<Track>();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + " !=0 ";
        String[] STAR = { "*" };
        Cursor cursor = context.getContentResolver().query(uri,STAR,selection,null,null);

        if(cursor!=null){
            if(cursor.moveToFirst()){
                do{
                    String trackName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    String artistName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    String albumName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                    Track track = new Track(trackName,artistName,albumName);

                    trackList.add(track);

                } while(cursor.moveToNext());
            }
        }
        return trackList;
    }
}
